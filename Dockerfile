FROM node:lts-alpine

LABEL maintainer="Ayana Developers <devs@ayana.io>"

RUN apk --no-cache add git

#create user
RUN adduser -Sh /ayanadoc ayanadoc

#copy source
COPY . /ayanadoc

#fix perms
RUN chown -R ayanadoc /ayanadoc

#use user and workdir
USER ayanadoc
WORKDIR /ayanadoc

#run yarn
RUN yarn install --production && \
    yarn cache clean

CMD ["node", "/ayanadoc/src/index.js"]
