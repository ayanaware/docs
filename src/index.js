'use strict';

const { Bento, FSComponentLoader } = require('@ayana/bento');

const log = require('@ayana/logger').get('AyanaDoc');

const bento = new Bento();

(async () => {
	const fsloader = new FSComponentLoader();
	await fsloader.addDirectory(__dirname, 'components');

	await bento.addPlugin(fsloader);

	await bento.verify();
})().catch(e => {
	log.error(`Error while starting AyanaDoc:`);
	log.error(e);
	process.exit(1);
});

