'use strict';

const cp = require('child_process');

const log = require('@ayana/logger').get('Yarn');

class Yarn {
	constructor() {
		this.name = 'Yarn';
	}

	async handle(yarn) {
		return new Promise((resolve, reject) => {
			yarn.stdout.on('data', data => {
				log.info(`    ${data.toString().trim()}`);
			});
			yarn.stderr.on('data', data => {
				log.warn(`    ${data.toString().trim()}`);
			});

			// Check exit code and exit too when yarn failed
			yarn.on('close', code => {
				if (code === 0) {
					log.info(`Yarn exited with code 0`);
					resolve();
				} else {
					log.error(`Yarn exited with code ${code}`);
					reject(new Error(`Yarn exited with code ${code}`));
				}
			});
		});
	}

	async install(dir) {
		log.info('Running yarn install...');

		const yarn = cp.spawn('yarn', ['--prod'], {
			cwd: dir,
		});

		await this.handle(yarn);
	}

	async add(dir, dependency) {
		log.info(`Running yarn add ${dependency}...`);

		const yarn = cp.spawn('yarn', ['add', dependency], {
			cwd: dir,
		});

		await this.handle(yarn);
	}
}

module.exports = Yarn;
