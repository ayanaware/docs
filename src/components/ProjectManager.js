'use strict';

const fs = require('fs');
const path = require('path');
const util = require('util');

const access = util.promisify(fs.access);
const mkdir = util.promisify(fs.mkdir);
const writeFile = util.promisify(fs.writeFile);

const exists = async dir => {
	try {
		await access(dir, fs.constants.F_OK);
		return true;
	} catch (e) {
		return false;
	}
};

const log = require('@ayana/logger').get('ProjectManager');

class ProjectManager {
	constructor() {
		this.name = 'ProjectManager';

		this.dependencies = ['Git', 'TypeDoc', 'Yarn'];

		this.repositories = path.resolve('./repositories');

		this.projects = [];

		this.blobBaseUrls = new Map();
	}

	async onLoad() {
		if (!(await exists(this.repositories))) {
			await mkdir(this.repositories);
			log.info('Created folder for repositories');
		}

		const repositoryPackage = path.resolve(this.repositories, 'package.json');

		if (!(await exists(repositoryPackage))) {
			await writeFile(
				repositoryPackage,
				JSON.stringify({ name: 'repositories', version: '1.0.0', license: 'MIT' })
			);
		}
	}

	async addProject(name, remoteUrl, blobBaseUrl) {
		const git = this.api.getComponent('Git');
		await git.addRepository(name, path.resolve('./repositories', name), 'master', remoteUrl);

		const typeDoc = this.api.getComponent('TypeDoc');
		typeDoc.addSource(path.resolve('./repositories', name));

		this.projects.push(name);
		this.blobBaseUrls.set(name, blobBaseUrl);
	}

	async addUnlistedDependency(dependency) {
		const yarn = this.api.getComponent('Yarn');
		await yarn.add(this.repositories, dependency);
	}

	async prepareProjects() {
		if (process.env.SKIP_PREPARE) return;

		for (const name of this.projects) {
			const git = this.api.getComponent('Git');
			// eslint-disable-next-line no-await-in-loop
			if (!process.env.NO_GIT_UPDATE) await git.updateRepository(name);

			const yarn = this.api.getComponent('Yarn');
			// eslint-disable-next-line no-await-in-loop
			await yarn.install(path.resolve('./repositories', name));
		}
	}

	getProjects() {
		return this.projects;
	}

	getBlobBaseUrl(project) {
		return this.blobBaseUrls.get(project);
	}
}

module.exports = ProjectManager;
