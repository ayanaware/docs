'use strict';

const title = process.env.TITLE || 'Documentation';

const path = require('path');

const typedoc = require('typedoc');

const { NodeModules, TocFixer } = require('./plugins');

const log = require('@ayana/logger').get('TypeDoc');

class TypeDoc {
	constructor() {
		this.name = 'TypeDoc';

		this.target = 'es2017';
		this.module = 'commonjs';
		this.output = path.resolve('./docs');

		this.sources = [];
	}

	onLoad() {
		this.app = new typedoc.Application({
			mode: 'modules',
			target: this.target,
			module: this.module,
			experimentalDecorators: true,
			skipLibCheck: false,
			excludePrivate: true,
			exclude: [
				'**/node_modules/**',
				'**/internal/**',
				'**/examples/**',
				'**/*.spec.ts',
			],
			excludeExternals: true,
			includeDeclarations: true,
			name: title,
			readme: 'none',
			theme: path.resolve('./theme'),
			logger: msg => {
				log.info(`    ${msg}`);
			},
		});

		this.app.options.addDeclaration({ name: 'api', defaultValue: this.api });

		this.app.converter.addComponent('node-modules', NodeModules);
		this.app.renderer.addComponent('toc-fixer', TocFixer);
	}

	generate() {
		log.info(`Project Target: ${this.target}, Module: ${this.module}`);
		log.info('Generating documentation with TypeDoc...');

		const src = this.app.expandInputFiles(this.sources);
		const project = this.app.convert(src);
		this.app.generateDocs(project, this.output);
	}

	addSource(directory) {
		this.sources.push(directory);
	}
}

module.exports = TypeDoc;
