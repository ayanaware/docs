'use strict';

const { RendererComponent, Component } = require('typedoc/dist/lib/output/components');
const { PageEvent } = require('typedoc/dist/lib/output/events');
const { ReflectionKind } = require('typedoc/dist/lib/models/reflections/abstract');

class TocFixer extends RendererComponent {
	initialize() {
		this.listenTo(this.owner, PageEvent.BEGIN, this.onRenderBeginPage);

		this.modelPages = new Map();
	}

	onRenderBeginPage(page) {
		const model = page.model;
		if (model.kindOf && model.kindOf(ReflectionKind.SomeModule)) {
			this.modelPages.set(model.name, page);
		}

		if (model.document) {
			let parent = model;
			while (parent.document) parent = parent.parent;

			// We can assume this will be there as parent pages are rendered before childs
			const parentPage = this.modelPages.get(parent.name);

			page.toc = parentPage.toc;
		}
	}
}

// eslint-disable-next-line new-cap
Component({ name: 'toc-fixer' })(TocFixer);

module.exports = TocFixer;
