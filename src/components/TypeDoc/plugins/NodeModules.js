'use strict';

const fs = require('fs');
const path = require('path');

const { ReflectionKind } = require('typedoc/dist/lib/models/reflections/abstract');
const { Component, ConverterComponent } = require('typedoc/dist/lib/converter/components');
const { Converter } = require('typedoc/dist/lib/converter/converter');
const { CommentPlugin } = require('typedoc/dist/lib/converter/plugins/CommentPlugin');

const mdTitleRegex = /([A-Za-z0-9 ]+)/;

class NodeModules extends ConverterComponent {
	initialize() {
		this.listenTo(this.owner, {
			[Converter.EVENT_BEGIN]: this.onBegin,
			[Converter.EVENT_CREATE_DECLARATION]: this.onDeclaration,
			[Converter.EVENT_RESOLVE_BEGIN]: this.onBeginResolve,
			[Converter.EVENT_RESOLVE_END]: this.onEndResolve,
		});

		this.packageNames = new Map();
		this.projectRoots = new Map();
		this.sourceFilePackageNames = new Map();
	}

	onBegin() {
		this.api = this.application.options.getValue('api');

		this.moduleRenames = [];
	}

	determinePackageNames(reflection) {
		if (reflection.sources.length === 0) return;

		let name = 'Unknown';

		reflection.sources.forEach(source => {
			const file = source.fileName;

			for (const packageFolder of this.packageNames.keys()) {
				if (file.startsWith(packageFolder)) {
					this.sourceFilePackageNames.set(source.fileName, this.packageNames.get(packageFolder));
					name = this.packageNames.get(packageFolder);
					return;
				}
			}

			let folder = path.dirname(file);
			// eslint-disable-next-line no-constant-condition
			while (true) {
				const pkgPath = path.resolve(folder, 'package.json');

				if (fs.existsSync(pkgPath)) {
					const pkg = require(pkgPath);
					// TODO Find a better way to detect our own package
					if (pkg.name === 'repositories') return name;

					this.packageNames.set(folder, pkg.name);
					this.projectRoots.set(pkg.name, folder);
					this.sourceFilePackageNames.set(source.fileName, pkg.name);
					name = pkg.name;
					return;
				}

				const n = path.dirname(folder);
				if (path === n) {
					this.sourceFilePackageNames.set(source.fileName, name);
					return;
				}

				folder = n;
			}
		});

		return name;
	}

	onDeclaration(context, reflection) {
		if (reflection.kindOf(ReflectionKind.ExternalModule)) {
			const packageName = this.determinePackageNames(reflection);
			this.moduleRenames.push({
				renameTo: packageName,
				reflection: reflection,
			});
		}
	}

	onBeginResolve(context) {
		const projRefs = context.project.reflections;
		const refsArray = Object.keys(projRefs).reduce((m, k) => {
			m.push(projRefs[k]);
			return m;
		}, []);

		const targets = [];

		// Process each rename
		this.moduleRenames.forEach(item => {
			const renaming = item.reflection;

			// Find an existing module that already has the "rename to" name.  Use it as the merge target.
			const mergeTarget = refsArray.filter(
				ref => ref.kind === renaming.kind && ref.name === item.renameTo,
			)[0];

			if (!mergeTarget) {
				// If there wasn't a merge target, just change the name of the current module and exit.
				renaming.name = item.renameTo;
				if (!renaming.children) renaming.children = [];
				targets.push(renaming);

				// Check all children of the newly created mergeTarget
				// .d.ts files usually have a declare module which creates a module which we don't want so we need this workaround
				for (const ref of renaming.children) {
					// Check for modules named like the node module with quotes
					if (ref.kind === ReflectionKind.Module && ref.name === `"${item.renameTo}"`) {
						// If one is found we move all children upwards to the mergeTarget
						for (const child of ref.children) {
							child.parent = renaming;
							renaming.children.push(child);
						}

						// Before removing we have to reset children
						ref.children = [];
						// Remove
						CommentPlugin.removeReflection(context.project, ref);
					}
				}

				return;
			}

			if (!mergeTarget.children) {
				mergeTarget.children = [];
			}

			// Since there is a merge target, relocate all the renaming module's children to the mergeTarget.
			const childrenOfRenamed = refsArray.filter(ref => ref.parent === renaming);
			childrenOfRenamed.forEach(ref => {
				// update links in both directions
				ref.parent = mergeTarget;
				mergeTarget.children.push(ref);
			});

			// Now that all the children have been relocated to the mergeTarget, delete the empty module
			// Make sure the module being renamed doesn't have children, or they will be deleted
			if (renaming.children) renaming.children.length = 0;
			CommentPlugin.removeReflection(context.project, renaming);

			// Remove @module and @preferred from the comment, if found.
			CommentPlugin.removeTags(mergeTarget.comment, 'module');
			CommentPlugin.removeTags(mergeTarget.comment, 'preferred');
		});

		// Targets are currently considered as external modules but we want them to show up as Modules
		targets.forEach(ref => {
			// console.log(require('util').inspect(ref, true, 0));
			ref.kind = ReflectionKind.Module;
		});
	}

	getMarkdownFiles(directory) {
		if (!fs.existsSync(directory)) return [];

		const dir = fs.readdirSync(directory);
		const markdownFiles = [];

		for (const file of dir) {
			const pth = path.resolve(directory, file);
			const stat = fs.statSync(pth);
			if (stat.isDirectory()) {
				markdownFiles.push(...this.getMarkdownFiles(pth));
			} else if (stat.isFile() && file.toLowerCase().endsWith('.md')) {
				markdownFiles.push(pth);
			}
		}

		return markdownFiles;
	}

	onEndResolve(context) {
		const project = context.project;

		const git = this.api.getComponent('Git');
		const pm = this.api.getComponent('ProjectManager');

		const projects = pm.getProjects();
		const projectGitMap = git.getCurrentCommits();

		// process mappings
		for (const sourceFile of project.files) {
			for (const pName of projects) {
				if (sourceFile.fileName.indexOf('/node_modules/') === -1 && sourceFile.fileName.startsWith(pName)) {
					sourceFile.url = sourceFile.fileName.replace(pName, `${pm.getBlobBaseUrl(pName)}${projectGitMap.get(pName)}`);
				}
			}
		}

		// add line anchors
		// eslint-disable-next-line guard-for-in
		for (const key in project.reflections) {
			const reflection = project.reflections[key];
			if (reflection.sources) {
				reflection.sources.forEach(source => {
					if (source.file) {
						if (source.file.url) {
							source.npmPackage = this.sourceFilePackageNames.get(source.file.fullFileName);

							for (const pName of projects) {
								if (source.fileName.startsWith(pName)) source.fileName = source.fileName.replace(pName, '');
							}

							if (source.fileName.startsWith('/lib')) source.fileName = source.fileName.replace('/lib', '');
							if (source.fileName.startsWith('/src')) source.fileName = source.fileName.replace('/src', '');
							while (source.fileName.startsWith('/')) source.fileName = source.fileName.replace('/', '');

							source.url = source.file.url + '#L' + source.line;
						} else {
							const split = source.file.fileName.split('/node_modules/');
							source.fileName = split[split.length - 1];
						}
					}
				});
			}
		}

		const reflections = project.getChildrenByKind(ReflectionKind.SomeModule);
		reflections.forEach(reflection => {
			reflection.documents = [];

			const projectRoot = this.projectRoots.get(reflection.name);

			// Check readme
			const dir = fs.readdirSync(projectRoot);
			const readme = dir.find(file => file.toLowerCase() === 'readme.md');
			if (readme) {
				const content = fs.readFileSync(path.resolve(projectRoot, readme), 'utf8');

				const doc = {
					name: 'Readme',
					content,
					beforeSubmodules: true,
					sortIndex: -1,
				};

				reflection.documents.push(doc);
			}

			// Check other documents
			const markdownFiles = this.getMarkdownFiles(path.resolve(projectRoot, 'docs'));
			for (const md of markdownFiles) {
				const content = fs.readFileSync(md, 'utf8');

				const title = mdTitleRegex.exec(content)[0].trim();

				const doc = {
					name: title,
					urlName: title.toLowerCase().replace(' ', '-'),
					content,
					beforeSubmodules: false,
				};

				reflection.documents.push(doc);
			}
		});
	}
}

// eslint-disable-next-line new-cap
Component({ name: 'node-modules' })(NodeModules);

module.exports = NodeModules;
