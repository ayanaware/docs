'use strict';

const fs = require('fs');
const path = require('path');
const util = require('util');

const simpleGit = require('simple-git/promise');

const access = util.promisify(fs.access);
const stat = util.promisify(fs.stat);
const mkdir = util.promisify(fs.mkdir);

const exists = async dir => {
	try {
		await access(dir, fs.constants.F_OK);
		return true;
	} catch (e) {
		return false;
	}
};

const log = require('@ayana/logger').get('Git');

class Git {
	constructor() {
		this.name = 'Git';

		this.repositories = new Map();
		this.currentCommits = new Map();
	}

	async addRepository(name, dir, branch, remoteUrl) {
		try {
			const stats = await stat(dir);

			if (!stats.isDirectory()) throw new Error('Repository path exists but is not a directory');
		} catch (e) {
			// Error is only thrown if the file/folder doesn't exists
			await mkdir(dir);
		}

		this.repositories.set(name, { dir, git: simpleGit(dir), branch, remoteUrl });

		log.info(`Added repository "${name}" with remote ${remoteUrl}`);
	}

	async updateCurrentCommits() {
		for (const name of this.repositories.keys()) {
			// eslint-disable-next-line no-await-in-loop
			this.currentCommits.set(name, await this.getCurrentCommit(name));
		}
	}

	getCurrentCommits() {
		return this.currentCommits;
	}

	async updateRepository(name) {
		if (!this.repositories.has(name)) throw new Error(`Repository ${name} does not exist`);
		log.info(`Updating repository ${name}`);

		const { dir, git, branch, remoteUrl } = this.repositories.get(name);

		if (await exists(path.resolve(dir, '.git'))) {
			log.info(`Local repository already exists! Fetching updates`);
			try {
				const remotes = await git.getRemotes(true);

				const remote = remotes[0];
				if (remote.refs.fetch !== remoteUrl) {
					throw new Error(`Repository url doesn't match remote url! Delete the repository folder to fix this`);
				}

				const result = await git.pull('origin', branch);

				log.info(`Pulled branch ${branch} from remote ${remoteUrl}`);
				// eslint-disable-next-line max-len
				log.info(`Summary: ${result.summary.changes} changes, ${result.summary.insertions} insertions, ${result.summary.deletions} deletions`);
			} catch (e) {
				throw new Error(`Failed to pull from git repository: ${e}`);
			}
		} else {
			log.info(`Local repository does not exist yet. Cloning from ${remoteUrl}`);
			try {
				await git.clone(remoteUrl, dir);
				log.info(`Cloned git repository`);
			} catch (e) {
				throw new Error(`Failed to clone git repository: ${e}`);
			}
		}

		await git.reset('hard');

		log.info(`Switching to branch ${branch}`);
		await git.checkout(branch);

		this.currentCommits.set(name, await this.getCurrentCommit(name));
	}

	async getCurrentCommit(name) {
		if (!this.repositories.has(name)) throw new Error(`Repository ${name} does not exist`);

		const { git } = this.repositories.get(name);

		const repoLog = await git.log({
			n: 1,
		});

		return repoLog.latest.hash;
	}

	async updateRepositoryToCommit(name, commitId) {
		if (!this.repositories.has(name)) throw new Error(`Repository ${name} does not exist`);
		log.info(`Updating repository ${name} to commit ${commitId}`);

		const { git } = this.repositories.get(name);

		const repoLog = await git.log({
			n: 1,
		});

		const commit = repoLog.latest;

		// eslint-disable-next-line max-len
		log.info(`Current commit is "${commit.message.replace('\n', ' ').trim()}" by "${commit.author_name} <${commit.author_email}>"`);
		log.info(`Commit ID is ${commit.hash}`);

		if (commitId != null && commitId.trim().length > 0 && commitId !== commit.hash) {
			log.info(`Requested commit doesn't match latest commit! Updating to requested commit`);
			await git.reset(['--hard', commitId]);
			log.info(`Finished resetting`);

			const repoLog2 = await git.log({
				n: 1,
			});

			const requestedCommit = repoLog2.latest;

			// eslint-disable-next-line max-len
			log.info(`Current commit is "${requestedCommit.message.replace('\n', ' ').trim()}" by "${requestedCommit.author_name} <${requestedCommit.author_email}>"`);
			log.info(`Commit ID is ${requestedCommit.hash}`);

			this.currentCommits.set(name, await this.getCurrentCommit(name));
			return requestedCommit.hash;
		}

		log.info('Requested commit and latest commit are identical');

		this.currentCommits.set(name, await this.getCurrentCommit(name));
		return commit.hash;
	}
}

module.exports = Git;
