'use strict';

const serverPort = (!process.env.SERVER_PORT || process.env.SERVER_PORT.length === 0 || isNaN(process.env.SERVER_PORT)) ?
	8080 : parseInt(process.env.SERVER_PORT, 10);

const path = require('path');

const server = require('http-server');

const log = require('@ayana/logger').get('DocManager');

class DocManager {
	constructor() {
		this.name = 'DocManager';

		this.dependencies = ['ProjectManager', 'TypeDoc', 'Yarn'];
	}

	async onLoad() {
		const pm = this.api.getComponent('ProjectManager');

		await pm.addProject('@ayana_bento', 'https://gitlab.com/ayana/libs/bento.git', 'https://gitlab.com/ayana/libs/bento/blob/');
		await pm.addProject('@ayana_di', 'https://gitlab.com/ayana/libs/di.git', 'https://gitlab.com/ayana/libs/di/blob/');
		await pm.addProject('@ayana_logger', 'https://gitlab.com/ayana/libs/logger.git', 'https://gitlab.com/ayana/libs/logger/blob/');
		await pm.addProject('@ayana_errors', 'https://gitlab.com/ayana/libs/errors.git', 'https://gitlab.com/ayana/libs/errors/blob/');

		await pm.prepareProjects();

		const typeDoc = this.api.getComponent('TypeDoc');

		await typeDoc.generate();

		server.createServer({
			root: path.resolve('./docs'),
			cache: 0,
		}).listen(serverPort);

		log.info(`Started HTTP server on port ${serverPort}`);
	}
}

module.exports = DocManager;
