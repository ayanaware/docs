AyanaWare [![NPM](https://img.shields.io/badge/npm-%40ayana-c63b68.svg)](https://www.npmjs.com/search?q=%40ayana) [![Discord](https://discordapp.com/api/guilds/508903834853310474/embed.png)](https://discord.gg/eaa5pYf)
===
AyanaWare is a collection of open source libraries, frameworks, and applications created and maintained by the Ayana Developers.
